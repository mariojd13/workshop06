<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//Esto es para sacar el ID del Usuario

$CI =& get_instance();

if ($this->uri->segment(3) == 0) {
  $user[0]['id'] = "" ;
  $user[0]['username'] = "" ;
  $user[0]['password'] = "" ;
  $user[0]['name'] = "" ;
  $user[0]['lastname'] = "" ;
}else{
  $CI->db->where('id', $this->uri->segment(3));
  $CI->db->get('users')->result_array();
  
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
 <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1>User Dashboard</h1>
      </div>
    </div>
    <div class="row">
        <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">Add User</div>
          <div class="panel-body">
            <form action="<?php echo base_url('User/saveUser')?>" method="post">
              <!-----------------------------------------------------!-->
              <div class="col-md-12 form-group input-group">
                <label for="" class="input-group-addon">Id:</label>
                <input type="text" name="id" class="form-control" value="<?php $dataUser['0'] ['id'] ?>">
              </div>
              <!-----------------------------------------------------!-->
              <div class="col-md-12 form-group input-group">
                <label for="" class="input-group-addon">Username:</label>
                <input type="text" name="username" class="form-control">
              </div>
              <div class="col-md-12 form-group input-group">
                <label for="" class="input-group-addon">Password:</label>
                <input type="password" name="password" class="form-control">
              </div>
              <div class="col-md-12 form-group input-group">
                <label for="" class="input-group-addon">Name:</label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-12 form-group input-group">
                <label for="" class="input-group-addon">Last Name:</label>
                <input type="text" name="lastname" class="form-control">
              </div>
              <!--País
              <div class="col-md-12 form-group input-group">
                <label for="" class="input-group-addon">Name:</label>
                <input type="text" name="name" class="form-control">
              </div>!-->
              <div>
              <div class="col-md-12 text-center">
              <a href="<?php echo base_url("user/dashboard")?>" class="btn btn-primary">New User</a>
              <button type="submit" class="btn btn-success">Add User</button>
              </div>
              </div>
            </form>
          </div>
        </div>

        </div>
        <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-heading">User Adding</div>
          <div class="panel-body">
            <table class="table table-hover table-striped">
              <thead>
                <th>Id</th>
                <th>User Name</th>
                <th>Name</th>
                <th>Last Name</th>
                <th>Actions</th>
                </thead>
              <tbody>
                <?php 
                  $CI =& get_instance();
                  $users = $CI->db->get('users')->result_array();
                  foreach ($users as $user) {
                    $edit = base_url("/User/saveUser/{$user['id']}");
                    $delete = base_url("User/deleteUser?delete={$user['id']}");
                    echo "<tr>
                    <td>{$user['id']}</td>
                    <td>{$user['username']}</td>
                    <td>{$user['name']}</td>
                    <td>{$user['lastname']}</td>

                    <td>
                      <a href='{$edit}' class='btn btn-info'>Edit</a>
                      <a href='{$delete}' onclick='return confirm(\"Do you want to delete user?\")' class='btn btn-danger'>Delete</a>
                    </td>
                    </tr>
                    ";
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
 </div>
</body>
</html>