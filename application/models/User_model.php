<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function authenticate($username, $password){
      $query = $this->db->get_where('users', array('username' => $username, 'password' => $password));
      if ($query->result()) {
        return $query->result()[0];
      } else {
        return false;
      }
  }

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function getByName($name){
      $query = $this->db->get_where('users', array('name' => $name));
      if ($query->result()) {
        return $query->result();
      } else {
        return false;
      }
  }

  /**
   *  Get user by Id
   *
   * @param $id  The user's id
   */
  public function getById($id){
      $query = $this->db->get_where('users', array('id' => $id));
      if ($query->result()) {
        return $query->result();
      } else {
        return false;
      }
  }

  /**
   *  Get all users from the databas
   *
   */
  public function all(){
      $query = $this->db->get('users');
      return $query->result();
  }

  function saveUsers($post){

    $dataUser = array();
    $dataUser['username'] = $post['username'];
    $dataUser['password'] = $post['password'];
    $dataUser['name'] = $post['name'];
    $dataUser['lastname'] = $post['lastname'];

    if ($dataUser['id'] >0) {
      $this->db->where('id', $dataUser['id']);
      $this->db->update('users');
      echo "<script>
        alert('User successfully updated');
      </script>"; 
    }
      $this->db->insert('users', $dataUser);    
      echo "<script>
        alert('User added successfully');
      </script>"; 
  }

  function deleteUser($get){
    $this->db->where('id', $get['delete']);
    $this->db->delete('users');
    echo "<script>
        alert('User successfully deleted');
      </script>"; 

  }

}
